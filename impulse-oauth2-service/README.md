**generate jks file**

keytool -genkeypair -alias jwt -keyalg RSA -dname "CN=jwt, L=Paris, S=Paris, C=FR" -keypass Impulse2018 -keystore jwt.jks -storepass Impulse2018

Extract the public key (only the public key , without the certificate) into file jwt.pub
keytool -list -rfc -alias jwt -keypass Impulse2018 -keystore jwt.jks -storepass Impulse2018 | openssl x509 -inform pem -pubkey -noout > jwt.pub

Put the content of the jwt.pub file in each MS :

        key-value: |
          -----BEGIN PUBLIC KEY-----
          MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0OIcJ+scLFXw23UtBfhI
          v3jqJ5qIivlZdL3T1gP3qZKaNicPvIVk1ozBJUan1hhUQE1gTI3PGi+Uhjb7zvvP
          GQnPR/nn3SzqtDGus99p5TVpMxNEQuPlMNVTQMcOOeWY2XoPTFFcWHgJJi0M1Fuc
          2KdbodvuT/h5FtRKQeTitAW1O0eZn9WUmYw9BD2N9Ijry3xaoA/rMvNDMWUBYlMH
          sicV/pUxK+t6EnMbUK1N7SHaXzZqEANLwP1ujuXdtJasYBg/Dk+2IIF7IV6MUKFh
          96xO+blmodP8GN+UtRicIHrQkFGuBh/slROluMPZSse0ISYmhJXPTRMNwmffEt/f
          FwIDAQAB
          -----END PUBLIC KEY-----

And copy the jwt.jks file to your Oauth2 project


**generate a user token**

curl -XPOST "http://localhost:2000/oauth/token" -d "client_id=drupal&client_secret=secret&grant_type=password&scope=Impulse&realm=clim&username=email@server.com&password=password"

curl -XPOST "http://localhost:2000/oauth/token" -d "client_id=drupal&client_secret=secret&grant_type=refresh_token&realm=clim&refresh_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzb2ZpYW5lLmxhb3VpbmlAZ21haWwuY29tIiwic2NvcGUiOlsiRGFpbHltZWQiXSwiYXRpIjoiYmU2ZTFjODUtNjBkMS00NzY5LWIwNTMtODEyMmNiNGFjMTNmIiwiZXhwIjoxNDkyMjgxNDcwLCJhdXRob3JpdGllcyI6WyJST0xFX01FTUJFUiJdLCJqdGkiOiIwOWFmNDU1MC0wYWVlLTQyNjgtOTU5ZC0zZmFlYTI2ZmUxMDMiLCJjbGllbnRfaWQiOiJ3ZWJfYXBwIn0.leSrI6pRLEylNBjYQv3dMlPLRmDJ6Q_oFbnerkY8c5_lf0N2iamb49mennypqpvnTE8ETss0wal3S-SfhIqCzSrW604UPf0peYpLNWW5xgjunKytI5gOHOKROEypcGGZ7809jfc14jqWaG6HdRZLSlqwk38tu4qu9cn8RMvdTSoU25d-C0JXGlZo5KuejRreYoKu3r7hzSbkqjs7FBaR10KLZ11N3Ait372DPYmGS5co6Uq9LP7L4mDscw9EXPAChIjs7Xhl-z7nMN5jhIppgCTeGkUWLBLHYQXDiK2K8SgWOcHHKseoWokQxmI1CtT6AXynesL5TEaiJRWFw6NbZA"


**generate application token**

curl -XPOST "http://localhost:2000/oauth/token" -d "client_id=drupal&client_secret=secret&grant_type=client_credentials&scope=Impulse"

Sample :

curl -XPOST "http://docker4sg.nonprod.cloudwatt.saint-gobain.net:42010/impulse-oauth2-service/oauth/token" \
  -d "client_id=drupal&client_secret=secret&grant_type=client_credentials&scope=Impulse" \
  | jq -r '.access_token' > token

curl -G "http://docker4sg.nonprod.cloudwatt.saint-gobain.net:42010/impulse-customer-query/api/v1/customers/professions" \
  -H "Authorization: Bearer $(cat token)" \
  -d "websiteId=clim" \
    | jq

**check token**
curl -XGET -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjIzMjU0ODAsInVzZXJfbmFtZSI6InNvZmlhbmUubGFvdWluaUBnbWFpbC5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX01FTUJFUiJdLCJqdGkiOiIxODg4ZGUyNy0wODU4LTQ2NmMtYTVkOC02ZjMyNjY3ZGE3YjMiLCJjbGllbnRfaWQiOiJ3ZWJfYXBwIiwic2NvcGUiOlsiRGFpbHltZWQiXX0.StKXJJs31nI3MZbMW2_NbMji2_Tv8BXq0TMms1z4eOnuLLDUWqAGekB7LtT-jYi5MrQrTDwkly1ddokzIR81KtPJE2vvTZON7NqFtcV5LRk5nIoW2Fi_rYCJPMIUm-nt6qqw4S_joZ1i0WoOFblrjFEQE54IaqT2FzvqT1ojwb3qiI42rPrMRlPu1gKLlcH4DzUOKeP2r0vgfdQTATnrzgg1Qn3cIFUjz7zX95rGo-LFxhHylMJbXnEuJ80zdx3HjSBZ2cgXSQ9ZF5yqFVrnWKQSSCYVCq0GgMLbFN1fJ7Z0OFOJOQ5AKGMv3cbM9sdQzt7DBRuFDJm6PAaDipAFEA" "http://localhost:8080/api/v1/some/resource"

**docker secret**

How to create secrets in docker :
docker secret create --label com.docker.ucp.access.label={{impulse.env}} {{impulse.secret.name}} {{impulse.secret.file.path}}
(you need to get and source in your shell the docker ucp key bundle from your profile before)

how to create the secrets of Impulse Oauth2 :

 export VERSION=v1
 export ENV=dev
 export FILENAME=oauth2-$ENV-$VERSION
 keytool -genkeypair -alias jwt -keyalg RSA -dname "CN=jwt, L=Paris, S=Paris, C=FR" -keypass Impulse2018 -keystore $FILENAME.jks -storepass Impulse2018
 keytool -list -rfc -alias jwt -keypass Impulse2018 -keystore $FILENAME.jks -storepass Impulse2018 | openssl x509 -inform pem -pubkey -noout > $FILENAME.pem
 docker secret create --label com.docker.ucp.access.label=impulse-$ENV $FILENAME.jks ./$FILENAME.jks
 docker secret create --label com.docker.ucp.access.label=impulse-$ENV $FILENAME.pem ./$FILENAME.pem

Please note :
- the key/keystore password must be Impulse2018 (this is not a secret)
- the Drupal frontend currently need the value of the public key => it must be configured in the Drupal Backoffice


Sample compose file with secrets :

version: '3.1'

services:
    oauth2:
        image: {{impulse.docker.dtr.url}}/impulse-oauth2-service:${impulse.oauth2.version}
        environment:
            ## ...
        secrets:
            - oauth2-v1.jks
            - oauth2-v1.pem

    agency-query:
        image: {{impulse.docker.dtr.url}}/impulse-agency-query:${impulse.agency.query.version}
        environment:
            ## ...
        secrets:
            - oauth2-v1.pem

secrets:
  oauth2-v1.jks:
    external: true
  oauth2-v1.pem:
    external: true
    
**delete certificat if exists**    
keytool -delete -alias apimgr.test.cer -keystore $JAVA_HOME/jre/lib/security/cacerts
    
**import certificat to test SSO in your local**
keytool -noprompt -keystore $JAVA_HOME/jre/lib/security/cacerts -importcert -alias apimgr.test.cer -file ./apimgr.test.cer -storepass changeit   
