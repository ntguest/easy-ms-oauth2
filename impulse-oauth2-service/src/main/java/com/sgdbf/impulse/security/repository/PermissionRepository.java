package com.sgdbf.impulse.security.repository;

import com.sgdbf.impulse.security.domain.Permission;
import com.sgdbf.impulse.security.domain.PermissionType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PermissionRepository extends MongoRepository<Permission, String> {

    List<Permission> findByIdInAndType(List<String> ids, PermissionType type);
}
