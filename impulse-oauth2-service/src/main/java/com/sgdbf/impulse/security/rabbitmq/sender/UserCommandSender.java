package com.sgdbf.impulse.security.rabbitmq.sender;

import com.sgdbf.impulse.common.domain.rabbit.FunctionalDomainEnum;
import com.sgdbf.impulse.common.domain.rabbit.FunctionalEntityEnum;
import com.sgdbf.impulse.rabbitmq.MessageEnvelope;
import com.sgdbf.impulse.rabbitmq.PojoWrapper;
import com.sgdbf.impulse.rabbitmq.outbound.RabbitMqRequestsOutbound;
import com.sgdbf.impulse.security.converter.OAuthUserConverter;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.dto.OAuthUserDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import static com.sgdbf.impulse.common.domain.utils.Constants.IMPULSE_USER_COMMAND;

/**
 * @author sla.
 */
@Component
@AllArgsConstructor
public class UserCommandSender {

    private final RabbitMqRequestsOutbound<OAuthUserDto> requestsOutbound;

    public void send(String websiteId, OAuthUser oAuthUser) {
        OAuthUserDto dto = OAuthUserConverter.newInstance().apply(oAuthUser);
        PojoWrapper<OAuthUserDto> pojoWrapper = new PojoWrapper<>(dto, PojoWrapper.EventType.STATS);
        MessageEnvelope<OAuthUserDto> envelope = new MessageEnvelope<>(websiteId, dto.getId(),
                pojoWrapper, IMPULSE_USER_COMMAND, FunctionalDomainEnum.User, FunctionalEntityEnum.OAuth);
        requestsOutbound.send(envelope);
    }
}
