package com.sgdbf.impulse.security.utils.data;

import com.sgdbf.impulse.common.ms.mongo.InitialLoad;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author yfo.
 */
@Slf4j
@Component
@Profile("docker")
public class ProdInitialLoad implements InitialLoad {

    @Override
    public void start() {
        // do nothing
    }
}
