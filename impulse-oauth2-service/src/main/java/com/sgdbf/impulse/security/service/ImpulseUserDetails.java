package com.sgdbf.impulse.security.service;

import com.google.common.annotations.VisibleForTesting;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * @author yfo.
 */
@Data
public class ImpulseUserDetails extends User {

    private String userId;
    private String accountId;
    private String login;
    private String firstName;
    private String lastName;
    private List<String> websites;
    private List<String> roles;
    private boolean impersonator;
    private String sgId;

    public ImpulseUserDetails(String username, String password, boolean enabled, Boolean emailValidation, List<GrantedAuthority> authorityList) {
        super(username, password, enabled, true, true, emailValidation, authorityList);
    }

    @VisibleForTesting
    public ImpulseUserDetails(String accountId, List<GrantedAuthority> authorityList, List<String> websites) {
        this(accountId, EMPTY, true, true, authorityList);
        this.websites = websites;
    }
}
