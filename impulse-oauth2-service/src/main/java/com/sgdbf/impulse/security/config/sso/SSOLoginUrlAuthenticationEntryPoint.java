package com.sgdbf.impulse.security.config.sso;

import com.sgdbf.impulse.security.config.RealmContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2SsoProperties;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yfo.
 */
@Component
public class SSOLoginUrlAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    @Value("${impulse.gateway.oauth2.url}")
    private String gatewayUrl;

    public SSOLoginUrlAuthenticationEntryPoint() {
        super(OAuth2SsoProperties.DEFAULT_LOGIN_PATH);
    }

    public String buildRedirectUrlToLoginPage(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {
        return gatewayUrl + OAuth2SsoProperties.DEFAULT_LOGIN_PATH + "?realm=" + RealmContext.getCurrentRealm();
    }
}
