package com.sgdbf.impulse.security.service;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.common.utils.DateUtils;
import com.sgdbf.impulse.security.config.OAuthProperties;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.utils.TokenHelper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.sgdbf.impulse.security.utils.Constants.AROBASE;

/**
 * @author yfo.
 */
@Component
@RequiredArgsConstructor
public class ImpersonationService {

    private final DefaultTokenServices defaultTokenServices;
    private final TokenHelper tokenHelper;
    private final OAuthUserService oAuthUserService;
    private final OAuthProperties properties;

    public DefaultOAuth2AccessToken getImpersonatedOAuth2AccessToken(OAuthUser user, String realm, String accountId, List<String> defaultRoles) {
        OAuth2Authentication authentication = getImpersonatedAuthentication(user, realm, accountId, defaultRoles);
        DefaultOAuth2AccessToken accessToken = (DefaultOAuth2AccessToken) defaultTokenServices.createAccessToken(authentication);
        Optional.ofNullable(properties.getToken().getImpersonationExpirationTime())
                .filter(seconds -> seconds > 0)
                .ifPresent(seconds -> accessToken.setExpiration(DateUtils.convert(LocalDateTime.now().plusSeconds(seconds))));
        accessToken.setRefreshToken(null);
        return accessToken;
    }

    private OAuth2Authentication getImpersonatedAuthentication(OAuthUser user, String realm, String accountId, List<String> defaultRoles) {
        OAuthUser oAuthUser = Optional.ofNullable(user).orElse(getDefaultUser(accountId, realm, defaultRoles));
        List<GrantedAuthority> authorities = oAuthUserService.findRolesWithReadPermissions(oAuthUser.getRoles());
        return new OAuth2Authentication(TokenHelper.getOAuth2Request(), getUserAuthentication(oAuthUser, authorities));
    }

    private UsernamePasswordAuthenticationToken getUserAuthentication(OAuthUser oAuthUser, List<GrantedAuthority> authorities) {
        ImpulseUserDetails userDetails = tokenHelper.getImpulseUserDetailsPermissions(oAuthUser, getReadOnlyPermissions(authorities));
        userDetails.setImpersonator(true);
        userDetails.setSgId(SecurityContextHolder.getContext().getAuthentication().getName());
        return new UsernamePasswordAuthenticationToken(userDetails, StringUtils.EMPTY, authorities);
    }

    private OAuthUser getDefaultUser(String accountId, String realm, List<String> defaultRoles) {
        OAuthUser user = new OAuthUser();
        user.setId(accountId + AROBASE + realm);
        user.setAccountId(accountId);
        user.setWebSites(Lists.newArrayList(realm));
        user.setRoles(defaultRoles);
        user.setPassword(StringUtils.EMPTY);
        user.setEmailValidation(true);
        user.setIsActive(true);
        return user;
    }

    private List<String> getReadOnlyPermissions(List<GrantedAuthority> authorities) {
        return authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
    }
}
