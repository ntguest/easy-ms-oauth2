package com.sgdbf.impulse.security.rabbitmq;

import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.rabbitmq.sender.UserCommandSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author yfo.
 */
@RequiredArgsConstructor
@Component
public class OAuthRemoteService implements RemoteService<OAuthUser> {

    private final UserCommandSender userCommandSender;

    @Override
    public void syncUserCommand(String websiteId, OAuthUser oAuthUser) {
        userCommandSender.send(websiteId, oAuthUser);
    }
}
