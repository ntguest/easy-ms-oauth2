package com.sgdbf.impulse.security.config;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.service.OAuthUserService;
import com.sgdbf.impulse.security.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

/**
 * @author obe.
 */
@RequiredArgsConstructor
public class ImpulseDaoAuthenticationProvider extends DaoAuthenticationProvider {

    private final OAuthUserService oAuthUserService;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (userDetails.getPassword().startsWith(Constants.BECRYPTED_PASSWORD_PREFIX)) {
            super.additionalAuthenticationChecks(userDetails, authentication);
            return;
        }
        additionalAuthenticationChecksForFirstLoginWithUpperCasePassword(userDetails, authentication);
    }

    private void additionalAuthenticationChecksForFirstLoginWithUpperCasePassword(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {
        String presentedPassword = authentication.getCredentials().toString();

        super.additionalAuthenticationChecks(userDetails, new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), presentedPassword.toUpperCase(), authentication.getAuthorities()));

        // presented password is valid, we can override Uppercase version with the presented password
        Optional<OAuthUser> oAuthUser = oAuthUserService.findOAuthUserByIdOrLoginAndWebsitesId(userDetails.getUsername(), Lists.newArrayList(RealmContext.getCurrentRealm()));
        oAuthUser.ifPresent(oAuthUserToUpdate -> updateUserPassword(oAuthUserToUpdate, presentedPassword));
    }

    private void updateUserPassword(OAuthUser oAuthUser, String newPassword) {
        oAuthUser.setPassword(passwordEncoder.encode(newPassword));
        oAuthUserService.update(oAuthUser);
    }
}
