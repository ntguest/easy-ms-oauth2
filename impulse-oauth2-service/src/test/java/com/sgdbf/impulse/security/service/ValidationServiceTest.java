package com.sgdbf.impulse.security.service;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.common.test.AbstractBaseTest;
import com.sgdbf.impulse.common.test.utils.TestUtils;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.utils.TokenHelper;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResourceAccessException;

import javax.inject.Inject;

import static com.sgdbf.impulse.common.ms.utils.Constants.ACL_ADMIN;
import static com.sgdbf.impulse.common.ms.utils.Constants.ROLE_ADMIN_DOD;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * @author yfo.
 */
@RunWith(SpringRunner.class)
@SpringBootTest("validationService")
public class ValidationServiceTest extends AbstractBaseTest {

    @Inject
    private ValidationService validationService;

    @Test
    public void should_validate_impersonation() {
        ImpulseUserDetails userDetails = new ImpulseUserDetails("VI_1234", "password", true, true, Lists.newArrayList());
        userDetails.setWebsites(Lists.newArrayList("default"));
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(userDetails, "");
        OAuth2Authentication auth2Authentication = new OAuth2Authentication(null, testingAuthenticationToken);
        validationService.validateImpersonation(auth2Authentication, getOAuthUser(), "default");
    }

    @Test
    public void should_validate_impersonation_when_user_is_client_authentication_with_admin_acl() {
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken("swagger", "", AuthorityUtils.createAuthorityList(ACL_ADMIN));
        OAuth2Authentication auth2Authentication = new OAuth2Authentication(null, testingAuthenticationToken);
        validationService.validateImpersonation(auth2Authentication, getOAuthUser(), "default");
    }

    @Test
    public void should_throw_resource_access_exception_when_user_is_client_authentication_with_admin_dod() {
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken("drupal", "", AuthorityUtils.createAuthorityList("TRUSTED_FRONT"));
        OAuth2Authentication auth2Authentication = new OAuth2Authentication(null, testingAuthenticationToken);
        Throwable throwable = catchThrowable(() -> validationService.validateImpersonation(auth2Authentication, getOAuthUser(), "default"));
        TestUtils.assertExceptionMessages(throwable, ResourceAccessException.class, Lists.newArrayList(ImpulseMessage.connection_as_not_allowed_to_realm.getKey()));
    }

    @Test
    public void should_throw_resource_access_exception() {
        ImpulseUserDetails userDetails = new ImpulseUserDetails("VI_1234", "password", true, true, AuthorityUtils.createAuthorityList(ROLE_ADMIN_DOD));
        userDetails.setWebsites(Lists.newArrayList("default"));
        OAuthUser user = getOAuthUser();
        user.setRoles(Lists.newArrayList("ADMIN_DOD"));
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(userDetails, "");
        OAuth2Authentication auth2Authentication = new OAuth2Authentication(TokenHelper.getOAuth2Request(), testingAuthenticationToken);
        Throwable throwable = catchThrowable(() -> validationService.validateImpersonation(auth2Authentication, user, "not_allowed_realm"));
        TestUtils.assertExceptionMessages(throwable, ResourceAccessException.class, Lists.newArrayList(ImpulseMessage.connection_as_admin_not_allowed.getKey(), ImpulseMessage.connection_as_not_allowed_to_realm.getKey()));
    }

    private OAuthUser getOAuthUser() {
        OAuthUser user = new OAuthUser();
        user.setRoles(Lists.newArrayList("ACCOUNT"));
        return user;
    }
}